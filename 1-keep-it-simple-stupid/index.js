const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/test-db', { useNewUrlParser: true });

const john = { name: 'John Wick', birthdate: new Date() };

people = mongoose.model('person', { name: String, birthdate: Date });

// Create
people.create(john);