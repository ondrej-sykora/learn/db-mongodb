# Let's get started with KISS 

At the start we are going to 💋 Keep It Simple, Stupid. In the first section we will try to connect to our database and create, read, update and delete some record. Just so you can learn how it works and how to do it.

First initialize the project with `npm init -y` command with default values.

```cmd
> npm init -y
```
Now it's time to install some libraries. There are two basic ways how to work with MongoDB. You can use the official [MongoDB driver](https://www.npmjs.com/package/mongodb) and use the MongoClient to work with database or you can use the [Mongoose](http://mongoosejs.com/) object modeling tool and work with collections using models and defined schemas.

> The main differences between these you can find at [http://voidcanvas.com/mongoose-vs-mongodb-native](http://voidcanvas.com/mongoose-vs-mongodb-native).

We will be using the second approach with Mongoose so let's get install the Mongoose package.

```cmd
> npm install mongoose --save
```

## Mongoose

Mongoose provides a straight-forward, schema-based solution to model your application data. It includes built-in type casting, validation, query building, business logic hooks and more, out of the box.

> More about that at [Mongoose official website](http://mongoosejs.com/).

### The fun begins 

Now we can think about what we really want to do. 

We want to:
* establish the connection
* store some data (Create)
* execute some operations over our data (Read, Update, Delete)

We can use the Mongoose `connect` function to make a db connection.

```javascript
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/test-db');
```

After that we can create some test data what we will be storing.

```javascript
const john = { name: 'John Wick', birthdate: new Date() };
```

Now can create the Mongoose `model`, it requires the `name` and the `schema` of the stored object.

```javascript
people = mongoose.model('person', { name: String, birthdate: Date });
```

And now we can insert our object into the MongoDB.

```javascript
people.create(john);
```

So your `index.js` file should looks like:

```javascript
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/test-db', { useNewUrlParser: true });

const john = { name: 'John Wick', birthdate: new Date() };

people = mongoose.model('person', { name: String, birthdate: Date });

// Create
people.create(john);
```

Now you can run the file, and check you MongoDB whether the data was successfully created.

> To run the file in the VisualStudio Code you can press `F5` and select the `Node.js` environment.

But that's not all, Mongoose `model` provides you several other useful methods that can help us in querying and manipulating data.

```javascript
// Read / Find all
people.find().then(result => console.log(result));

// Read / Find all
people.findById('5b56cb48c19a98343c5500c2').then(result => console.log(result));
// You can use new ObjectId('5b56cb48c19a98343c5500c2') as well

// Update
people.findByIdAndUpdate('5b56cb48c19a98343c5500c2', { name: 'Updated Name' });
// You can use new ObjectId('5b56cb48c19a98343c5500c2') as well

// Delete / Remove
people.findByIdAndRemove('5b56d2c50cd8e5052c372e28');
// You can use new ObjectId('5b56cb48c19a98343c5500c2') as well
```

> To check the MongoDB content you can use the **MongoDB Compass**.

All of these methods (`create()` too) are asynchronous and returns a `Promise`. You can handle the results by the `then()` or `await` if you want like it is shown with `find()` and `findById()` methods.

> You can read more about async and promises in article [Callbacks, Promises and Async/Await](https://medium.com/front-end-hacking/callbacks-promises-and-async-await-ad4756e01d90).

If you want to return the modified item rather than original after `update()`, you must set the `new` [option](http://mongoosejs.com/docs/api.html#model_Model.findByIdAndUpdate) to true, dfault is false.

👋 See you next time.