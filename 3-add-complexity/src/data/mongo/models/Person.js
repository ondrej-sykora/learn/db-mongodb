const mongoose = require('mongoose');
const idPlugin = require('../plugins/idPlugin');
const { AddressSchema } = require('./Address');
const Schema = mongoose.Schema;

const PersonModelName = 'Person';

const PersonSchema = new Schema({
    name: {
        type: mongoose.SchemaTypes.String,
        required: true
    },
    birthdate: mongoose.SchemaTypes.Date,
    address: AddressSchema
});
PersonSchema.plugin(idPlugin);

const PersonModel = mongoose.model(PersonModelName, PersonSchema);

module.exports = {
    PersonModel: PersonModel
}