class Address {
    constructor(city, street, country) {
        this.city = city;
        this.street = street;
        this.country = country;
    }
}

module.exports = Address;