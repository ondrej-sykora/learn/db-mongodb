const mongoose = require('mongoose');
const Person = require('./entities/Person');
const Address = require('./entities/Address');
const PersonRepository = require('./data/mongo/PersonRepository');

(async () => {
    // DeprecationWarning: current URL string parser is deprecated, and will be removed in a future version. To use the new parser, pass option { useNewUrlParser: true } to MongoClient.connect.
    mongoose.connect('mongodb://localhost:27017/test-db');

    const address = new Address('London', 'Friday Street', 'England');

    const people = new Array(
        new Person('Neo', new Date()),
        new Person('Morpheus', new Date(), address),
        new Person('Trinity', new Date(), address),
        new Person('Apoc', new Date()),
        new Person('Tank', new Date(), address),
        new Person('Agent Smith', new Date(), address)
    );

    const personRepo = new PersonRepository();

    people.forEach(person => personRepo.create(person).then(res => console.log(res)));

    personRepo.getAll().then(res => res.forEach(person => console.log(person)));
    // Test all metods of repository
})();