# Complexity is coming

In our previous example we built the application that uses the simple repository connected to the MongoDB database. But it was a really simple example where we worked with only single entity.

In this example, we are going go add ability to user to have an address. In the document data model that MongoDB uses we can attach the address directly to the person data object. That is bit easier than for example in data relational model.

We can use the previous example as starting point, so you can start by editing the current code or, you can copy paste the previous example to the separate folder. After copy don't forget install the npm packages into the new directory.

```cmd
> npm install
```

# Application Domain

So at first, we should create an `Address` class. And after that we shouldn't forget update our `Person` class and add the address property.

*./src/entities/Address.js*
```javascript
class Address {
    constructor(city, street, country) {
        this.city = city;
        this.street = street;
        this.country = country;
    }
}

module.exports = Address;
```

*./src/entities/Person.js*
```javascript
...
    constructor(name, birthdate, address) {
        this.name = name;
        this.birthdate = birthdate;

        if (address)
            this.address = address;
    }
...
```

# Data Layer

As we have updated our business logic's objects we can move to the data layer. Add Mongoose model for Address entity and after that you can update the person one.

*./src/data/mongo/models/Address.js*
```javascript
const mongoose = require('mongoose');
const idPlugin = require('../plugins/idPlugin');

const AddressSchema = new mongoose.Schema({
    city: mongoose.SchemaTypes.String,
    street: mongoose.SchemaTypes.String,
    country: mongoose.SchemaTypes.String
});
AddressSchema.plugin(idPlugin);

module.exports = {
    AddressSchema: AddressSchema
}
```

*./src/data/mongo/models/Person.js*
```javascript
const { AddressSchema } = require('./Address');
...
const PersonSchema = new Schema({
    name: {
        type: mongoose.SchemaTypes.String,
        required: true
    },
    birthdate: mongoose.SchemaTypes.Date,
    address: AddressSchema
});
...
```

Note that we are exporting only the address schema object. Thats is because we have decided store the address object together with the person. 

We would need the address model only in the case we would decide store the address in separate collection and link it to the person with some kind of relation. But that would rather be an example of a relational database.

# Application

Now it's again the right time to update the application to test changes we have done. For our purpose we don't must create different address for each person so we can use only one and attach it to selected users.

*./src/index.js*
```javascript
const mongoose = require('mongoose');
const Person = require('./entities/Person');
const Address = require('./entities/Address');
const PersonRepository = require('./data/mongo/PersonRepository');

(async () => {
    mongoose.connect('mongodb://localhost:27017/test-db');

    const address = new Address('London', 'Friday Street', 'England');

    const people = new Array(
        new Person('Neo', new Date()),
        new Person('Morpheus', new Date(), address),
        new Person('Trinity', new Date(), address),
        new Person('Apoc', new Date()),
        new Person('Tank', new Date(), address),
        new Person('Agent Smith', new Date(), address)
    );

    const personRepo = new PersonRepository();

    people.forEach(person => personRepo.create(person));
})();
```

After that you create these objects you can try to read them from the repository and boom... Addresses are here 🎉.

So this was the example of how to store some dependent objects on the MongoDB. You can imagine this as a 1:1 equivalent in the relationship model.

In the next part we will be implementing some 'relations' in the MongoDB world. Because there may be the situation where is the best solution to have two separate collections.

👋 See you next time.