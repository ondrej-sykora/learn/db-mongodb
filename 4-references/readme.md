

Now we can 'employ' people and gets all employee from company. Well we can get only their ids, or... 😏 Is there some magic we may use to retrieve entire employee data? 🤔

We can take a look to the Mongoose documentation and we can find the `Populate()` function.

> More info about Populate you can find in the [documentation](http://mongoosejs.com/docs/populate.html).

Now it's time to update the `PersonRepository` with the populate function. I will be the same as in `CompanyRepository` but instead of `employee` we will be populating the `employer`.

*./src/data/mongo/PersonRepository.js*
```javascript
const { PersonModel } = require('./models/Person');

class PersonRepository {
    create(person) {
        return PersonModel.create(person)
            .then(res => PersonModel.populate(res, { path: 'employer' }))
            .then(res => res.toJSON());
    }

    get(personId) {
        return PersonModel.findById(personId)
            .populate('employer')
            .then(res => res.toJSON());
    }
    ...  // The rest is the same as findById()
}

module.exports = PersonRepository;
```



*./src/index.js*
```javascript
...
(async () => {
    mongoose.connect('mongodb://localhost:27017/test-db');

    let company = new Company('Brand New Company');

    const personRepo = new PersonRepository();
    const companyRepo = new CompanyRepository();

    employees = await personRepo.getAll();
    company = await companyRepo.create(company)

    companyRepo.update({ ...company, employees: employees.map(emp => emp.id) })
        .then(company => {
            console.log(company);

            employees.forEach(employee => {
                personRepo.update({ ...employee, employer: company.id })
                    .then(person => console.log(person));
            });
        });
})();
```

In the debug output, you can see that the collection was actually populated with the objects instead of just ids.

So in this section, we created some relationships between two separated collections in the MongoDB database. You can imagine this as a 1:N equivalent in the relationship model.

That was the finall part of this tutorial abou how to work with MongoDB in Node.js. I hope you really enjoyed this series and next we'll be looking at how to work with MS SQL (MySQL or PostgreSQL) datasource in Node.js application.

👋 See you next time.