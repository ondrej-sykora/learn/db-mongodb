const mongoose = require('mongoose');
const idPlugin = require('../plugins/idPlugin');

// No need for separate collection 
// => Will be stored in the same document as parent
// const AddressModelName = 'Address';

const AddressSchema = new mongoose.Schema({
    city: mongoose.SchemaTypes.String,
    street: mongoose.SchemaTypes.String,
    country: mongoose.SchemaTypes.String
});
AddressSchema.plugin(idPlugin);

// No need for model 
// const AddressModel = mongoose.model(AddressModelName, AddressSchema);

module.exports = {
    AddressSchema: AddressSchema
}