const { CompanyModel } = require('./models/Company');

class CompanyRepository {
    create(company) {
        return CompanyModel.create(company)
            .then(res => CompanyModel.populate(res, { path: 'employees' }))
            .then(res => res.toJSON());
    }

    get(companyId) {
        return CompanyModel.findById(companyId)
            .populate('employees')
            .then(res => res.toJSON());
    }

    getAll() {
        return CompanyModel.find()
            .populate('employees')
            .then(res => res.map(resItem => resItem.toJSON()));
    }

    update(company) {
        return CompanyModel.findByIdAndUpdate(company.id, company, { new: true })
            .populate('employees')
            .then(res => res.toJSON());
    }

    delete(companyId) {
        return CompanyModel.findByIdAndRemove(companyId)
            .populate('employees')
            .then(res => res.toJSON());
    }
}

module.exports = CompanyRepository;