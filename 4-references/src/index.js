const mongoose = require('mongoose');
const Company = require('./entities/Company');
const PersonRepository = require('./data/mongo/PersonRepository');
const CompanyRepository = require('./data/mongo/CompanyRepository');

(async () => {
    // DeprecationWarning: current URL string parser is deprecated, and will be removed in a future version. To use the new parser, pass option { useNewUrlParser: true } to MongoClient.connect.
    mongoose.connect('mongodb://localhost:27017/test-db');

    let company = new Company('Brand New Company');

    const personRepo = new PersonRepository();
    const companyRepo = new CompanyRepository();

    employees = await personRepo.getAll();
    company = await companyRepo.create(company)

    companyRepo.update({ ...company, employees: employees.map(emp => emp.id) })
        .then(company => {
            console.log(company);

            employees.forEach(employee => {
                personRepo.update({ ...employee, employer: company.id })
                    .then(person => console.log(person));
            });
        });
})();