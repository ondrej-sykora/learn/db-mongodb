class Person {
    constructor(name, birthdate, address) {
        this.name = name;
        this.birthdate = birthdate;

        if (address)
            this.address = address;
    }
}

module.exports = Person;