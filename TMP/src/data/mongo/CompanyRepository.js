const { CompanyModel } = require('./models/Company');

class CompanyRepository {
    create(company) {
        return CompanyModel.create(company)
            .then(res => res.toJSON());
    }

    get(companyId) {
        return CompanyModel.findById(companyId)
            .then(res => res.toJSON());
    }

    getAll() {
        return CompanyModel.find()
            .then(res => res.map(resItem => resItem.toJSON()));
    }

    update(company) {
        return CompanyModel.findByIdAndUpdate(company.id, company, { new: true })
            .then(res => res.toJSON());
    }

    delete(companyId) {
        return CompanyModel.findByIdAndRemove(companyId)
            .then(res => res.toJSON());
    }

    getEmployees(companyId) {
        return CompanyModel.findById(companyId)
            .populate('employees')
            .then(res => res.employees ? res.employees : new Array());
    }

    hire(companyId, employeeId) {
        return CompanyModel.findByIdAndUpdate(companyId, { $addToSet: { employees: employeeId } }, { new: true })
            .populate('employees')
            .then(async (company) => {
                const employee = company.employees.find(emp => emp._id.equals(employeeId))

                // if (employee.employer) {
                //     await this.fire(employee.employer._id, employee._id);
                // }

                employee.employer = companyId;
                await employee.save();
            });
    }

    fire(companyId, employeeId) {
        return CompanyModel.findByIdAndUpdate(companyId, { $pull: { employees: employeeId } })
            .populate('employees')
            .then(async (company) => {
                const employee = company.employees.find(emp => emp._id.equals(employeeId))
                employee.employer = null;
                await employee.save();
            });
    }
}

module.exports = CompanyRepository;