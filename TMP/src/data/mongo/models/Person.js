const mongoose = require('mongoose');
const idPlugin = require('../plugins/idPlugin');
const { CompanyModelName, PersonModelName } = require('./ModelNames');
const { AddressSchema } = require('./Address');
const Schema = mongoose.Schema;

const PersonSchema = new Schema({
    name: {
        type: mongoose.SchemaTypes.String,
        required: true
    },
    birthdate: mongoose.SchemaTypes.Date,
    address: AddressSchema,
    employer: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: CompanyModelName,
        default: null
    }
});
PersonSchema.plugin(idPlugin);

const PersonModel = mongoose.model(PersonModelName, PersonSchema);

module.exports = {
    PersonModel: PersonModel,
}