const { PersonModel } = require('./models/Person');

class PersonRepository {
    create(person) {
        return PersonModel.create(person)
            .then(res => PersonModel.populate(res, { path: 'employer' }))
            .then(res => res.toJSON());
    }

    get(personId) {
        return PersonModel.findById(personId)
            .populate('employer')
            .then(res => res.toJSON());
    }

    getAll() {
        return PersonModel.find()
            .populate('employer')
            .then(res => res.map(resItem => resItem.toJSON()));
    }

    update(person) {
        return PersonModel.findByIdAndUpdate(person.id, person, { new: true })
            .populate('employer')
            .then(res => res.toJSON());
    }

    remove(personId) {
        return PersonModel.findByIdAndRemove(personId)
            .populate('employer')
            .then(res => res.toJSON());
    }
}

module.exports = PersonRepository;