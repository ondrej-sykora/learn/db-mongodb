const mongoose = require('mongoose');
const Company = require('./entities/Company');
const PersonRepository = require('./data/mongo/PersonRepository');
const CompanyRepository = require('./data/mongo/CompanyRepository');

(async () => {
    // DeprecationWarning: current URL string parser is deprecated, and will be removed in a future version. To use the new parser, pass option { useNewUrlParser: true } to MongoClient.connect.
    mongoose.connect('mongodb://localhost:27017/test-db');

    let company = new Company('Brand New Company');

    const personRepo = new PersonRepository();
    const companyRepo = new CompanyRepository();

    const people = await personRepo.getAll();
    // company = await companyRepo.create(company)

    // let randomPerson = people[Math.floor(Math.random() * people.length)];
    // company.employees.push();

    // await hire(companyRepo, personRepo, company.id, randomPerson.id);
    // companyRepo.update({ ...company, employees: company.employees.push(randomPerson.id) })
    //     .populate('employees')
    //     .then(company => {
    //         console.log(company);
    //     })

    // console.log(randomPerson);

    await companyRepo.hire('5b601208e2081e21c0f9a950', '5b5abcbcf1ed191e5c9376c3');
    // await companyRepo.hire(company.id, '5b5abcbcf1ed191e5c9376c8');
    // await companyRepo.fire('5b5efcfd32edbe40583e2b8b', '5b5abcbcf1ed191e5c9376c3');

    // personRepo.get('5b5abcbcf1ed191e5c9376c3').then(res => console.log(res));

    // companyRepo.getEmployees(company.id).then(res => console.log(res));

    // companyRepo.get(company.id).then(company => console.log(company));
    // companyRepo.update({ ...company, employees: employees.map(emp => emp.id) })
    //     .then(company => {
    //         console.log(company);

    //         employees.forEach(employee => {
    //             personRepo.update({ ...employee, employer: company.id })
    //                 .then(person => console.log(person));
    //         });
    //     });
})();