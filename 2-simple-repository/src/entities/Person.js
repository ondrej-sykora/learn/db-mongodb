class Person {
    constructor(name, birthdate, address) {
        this.name = name;
        this.birthdate = birthdate;
    }
}

module.exports = Person;