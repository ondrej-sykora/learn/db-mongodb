const idPlugin = require('../plugins/idPlugin');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PersonModelName = 'Person';

const PersonSchema = new Schema({
    name: {
        type: mongoose.SchemaTypes.String,
        required: true
    },
    birthdate: mongoose.SchemaTypes.Date
});
PersonSchema.plugin(idPlugin);

const PersonModel = mongoose.model(PersonModelName, PersonSchema);

module.exports = {
    PersonModel: PersonModel
}