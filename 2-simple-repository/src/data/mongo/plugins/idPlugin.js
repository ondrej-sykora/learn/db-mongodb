const idPlugin = (schema, options) => {
    schema.virtual('id').get(function () { return this._id; });
    schema.set('toObject', { virtuals: true });
    schema.set('toJSON', { virtuals: true });
    schema.set(
        'toJSON', {
            virtuals: true,
            transform: (doc, ret, opt) => {
                delete ret._id;
                delete ret.__v;
            }
        });
};

module.exports = idPlugin;