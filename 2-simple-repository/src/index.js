const mongoose = require('mongoose');
const Person = require('./entities/Person');
const PersonRepository = require('./data/mongo/PersonRepository');

(async () => {
    // DeprecationWarning: current URL string parser is deprecated, and will be removed in a future version. To use the new parser, pass option { useNewUrlParser: true } to MongoClient.connect.
    mongoose.connect('mongodb://localhost:27017/test-db');

    const chev = new Person('Chev Chelios', new Date());

    const personRepo = new PersonRepository();
    let result;

    result = await personRepo.create(chev);
    console.log(result);

    result = await personRepo.get(result.id);
    console.log(result);

    result = await personRepo.getAll();
    console.log(result);

    let randomPerson = result[Math.floor(Math.random() * result.length)];

    result = await personRepo.update({ ...randomPerson, name: 'Updated Name' });
    console.log(result);

    result = await personRepo.remove(randomPerson.id);
    console.log(result);
})();