# More practical example

So we know how to work with MongoDB now. But let me show you little bit practical example. if you didn't continue from the previous example, first initialize the project and install Mongoose package.

```cmd
> npm init -y
> npm install mongoose --save
```

## Folder structure

At start, let's create some folder structure.

```
.                       # Root (project) folder
├─ src                  # Source code of the application
│  ├─ index.js          # Application startup file
│  ├─ data              # All about data layer should be there
│  │   └─ mongo         # MongoDB files will be there
│  │      ├─ models     # MongoDB models folder
│  │      └─ plugins    # MongoDB plugins folder
│  └─ entities          # Domain objects
├─ package.json         # Project file (npm init -y)
└─ .gitignore           # Git ignore file
```

## Application Domain

The index.js will be our application startup point. As earlier, we will be working with two Person entity in our application domain. So we can also create ES class in the entities folder for it. I am OOP programmer so I love these new ES6 stuffs like classes and interfaces 😍.

*./src/entities/Person.js*
```javascript
class Person {
    constructor(name, birthdate) {
        this.name = name;
        this.birthdate = birthdate;
    }
}

module.exports = Person;
```

We created this domain object, because in our application (business logic) we don't really want to work directly with data object (data layer) returned from the database. So, although we will have to cast the object, the data layer and the business logic will be strictly separated in our app design.

## Data Layer

Now we can create the Mongoose model. In the previous example we did that by inline initialization, it was funkcional but it will be better to do that in the separate file.

*./src/data/mongo/models/Person.js*
```javascript
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PersonModelName = 'Person';

const PersonSchema = new Schema({
    name: {
        type: mongoose.SchemaTypes.String,
        required: true
    },
    birthdate: mongoose.SchemaTypes.Date
});

const PersonModel = mongoose.model(PersonModelName, PersonSchema);

module.exports = {
    PersonModel: PersonModel
}
```

So we created the schema separately, just for the good readability, and after that we created model and expose it from file. 

> You can read more about Mongoose schema and model at Mongoose [documentation](http://mongoosejs.com/docs/guides.html).

Because we are trying use the best of the OOP world so we need something to encapsulate the read / write logic. We can use some kind of repository, that will definitely saves us a lot of time when we decide to write some tests in the future. In this case we need repository for working with MongoDB datasource, so it could be placed somewhere in the `mongo` directory.

*./src/data/mongo/PersonRepository.js*
```javascript
const { PersonModel } = require('./models/Person');

class PersonRepository {
    create(person) {
        return PersonModel.create(person)
            .then(res => res.toJSON());
    }

    get(personId) {
        return PersonModel.findById(personId)
            .then(res => res.toJSON());
    }

    getAll() {
        return PersonModel.find()
            .then(res => res.map(resItem => resItem.toJSON()));
    }

    update(person) {
        return PersonModel.findByIdAndUpdate(person.id, person, { new: true })
            .then(res => res.toJSON());
    }

    remove(personId) {
        return PersonModel.findByIdAndRemove(personId)
            .then(res => res.toJSON());
    }
}

module.exports = PersonRepository;
```

Note that we can using the `toJSON()` method to return the plain javascript object that fits our `Person` domain object. As we mentioned above, we do not want our business logic to work with data layer objects.

## Application

Now we can try if our sample code works.

*./src/index.js*
```javascript
const mongoose = require('mongoose');
const Person = require('./entities/Person');
const PersonRepository = require('./data/mongo/PersonRepository');

(async () => {
    mongoose.connect('mongodb://localhost:27017/test-db');

    const chev = new Person('Chev Chelios', new Date());

    const personRepo = new PersonRepository();
    let result;

    result = await personRepo.create(chev);
    console.log(result);
})();
```

We are using the anonymous `async` function because we want to get result of `create()` method using `await` and then work with it. 
Ok, the user was created, now we can try the other methods of our simple repository. 

We just got the inserted record, but what if we want a different one.

```javascript
    result = await personRepo.get(result.id);
    console.log(result);
```

Hmmm... But where `id` comes from? 🤔

Well, the MongoDB uses the one field as primary key on elements in a collection across all of its content and it's called... `_id` 😦 The _id field is the unique naming convention that MongoDB uses. It's little bit strange in my opinion, so we need some way how to convert it to the id that we want to use.

For this purpose we can create the Mongoose plugin. Schemas are pluggable, that is, they allow for applying pre-packaged capabilities to extend their functionality.

> Mongose plugins [documentation](http://mongoosejs.com/docs/plugins.html).

*./src/data/mongo/plugins/idPlugin.js*
```javascript
const idPlugin = (schema, options) => {
    schema.virtual('id').get(function () { return this._id; });
    schema.set('toObject', { virtuals: true });
    schema.set('toJSON', { virtuals: true });
    schema.set(
        'toJSON', {
            virtuals: true,
            transform: (doc, ret, opt) => {
                delete ret._id;
                delete ret.__v;
            }
        });
};

module.exports = idPlugin;
```

At first plugin adds the virtual property `id` to our MongoDB document. This property will has the same value `_id`. The plugin also includes this property in the output of `toJSON()` method. And finally also removes unnecessary document properties from the `toJSON()` result.

Now it is time to integrate the plugin into our schema. So after schema is created add `PersonSchema.plugin(idPlugin);` line to your code. Of course do not forget to import our plugin file before.

*./src/data/mongo/models/Person.js*
```javascript
const idPlugin = require('../plugins/idPlugin');
...
const PersonSchema = new Schema({
    name: {
        type: mongoose.SchemaTypes.String,
        required: true
    },
    birthdate: mongoose.SchemaTypes.Date
});
PersonSchema.plugin(idPlugin);
...
```

Now we have everything done and we can test the remaining functionality of our repository.

```javascript
    result = await personRepo.getAll();
    console.log(result);

    let randomPerson = result[Math.floor(Math.random() * result.length)];

    result = await personRepo.update({ id: randomPerson.id, name: 'Updated Name' });
    console.log(result);

    result = await personRepo.remove(randomPerson.id);
    console.log(result);
```

Now we have working example with simple repository using MongoDB as datasource 🎉. We will also use this project next as our starting point and expand it a bit.

👋 See you next time.