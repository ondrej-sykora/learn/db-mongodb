# References

Related to the previous parts of this tutorial, we have a fully functional repository, that can works with users and their addresses.

But what can you do with a lot of unemployed people? And think of their families, what would they live for? 😢

So let's get them to work but first we need some companies to employ them. And it means... 🤔 Yes we must create the Company class first... 😄

*./src/entities/*
```javascript
class Company {
    constructor(name) {
        this.name = name;
    }
}

module.exports = Company;
```

Once we have Company class created we can create the Mongoose model for it.

*./src/data/mongo/models/Company.js*
```javascript
const mongoose = require('mongoose');
const { PersonModelName } = require('./Person');
const idPlugin = require('../plugins/idPlugin');
var Schema = mongoose.Schema;

const CompanyModelName = 'Company';

const CompanySchema = new Schema({
    name: {
        type: mongoose.SchemaTypes.String,
        required: true
    },
    employees: [{
        type: mongoose.SchemaTypes.ObjectId,
        ref: PersonModelName
    }]
});
CompanySchema.plugin(idPlugin);

const CompanyModel = mongoose.model(CompanyModelName, CompanySchema);

module.exports = {
    CompanyModel: CompanyModel,
    CompanyModelName: CompanyModelName
};
```

Note that the reference is just an array of type ObjectId, but we can specify the target model by `ref` property. That will be helpfull in the next steps.

But the model name is encapsulated in the Person model file so it's needed to be exposed. We can do that by adding it to the export together with the model itself.

*./src/data/mongo/models/Person.js*
```javascript
...

module.exports = {
    PersonModel: PersonModel,
    PersonModelName: PersonModelName
}
```

Ok, that was the way from compnay to the employees but si there any way how access the company from the employee? Yes it is, but at the first, we must update the model of the person. We could try something like that...

*./src/data/mongo/models/Person.js*
```javascript
const { CompanyModelName } = require('./Company');
...
const PersonSchema = new Schema({
    name: {
        type: mongoose.SchemaTypes.String,
        required: true
    },
    birthdate: mongoose.SchemaTypes.Date,
    address: AddressSchema,
    employer: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: CompanyModelName,
        default: null
    }
});
...
```

Hmm... But wait a minute ☝️ We have referenced `Person` model in the `Company` model because the `PersonModelName` and now we are referencing the `Company` model in the `Person` model... 🤔 This can't be right because there is a circular reference. Yeh the circular references are not good for many reasons and we should avoid them.

So we can place the model mames into the separate file and then referenc that file from both models. 😎

*./src/data/mongo/models/ModelNames.js*
```javascript
module.exports = {
    PersonModelName: 'Person',
    CompanyModelName: 'Company'
}
```

And now you can simply reference that without circular references like this...

```javascript
const { CompanyModelName, PersonModelName } = require('./ModelNames');
```

Obviously you can delete the definitions and exports of the `CompanyModelName` and the `PersonModelName` consts from the model files.

Ok, the last missing thing is the repository. So let's get create one or the Companies.

*./src/data/mongo/CompanyRepository.js*
```javascript
const { CompanyModel } = require('./models/Company');

class CompanyRepository {
    create(company) {
        return CompanyModel.create(company)
            .then(res => res.toJSON());
    }

    get(companyId) {
        return CompanyModel.findById(companyId)
            .then(res => res.toJSON());
    }

    getAll() {
        return CompanyModel.find()
            .then(res => res.map(resItem => resItem.toJSON()));
    }

    update(company) {
        return CompanyModel.findByIdAndUpdate(company.id, company, { new: true })
            .then(res => res.toJSON());
    }

    delete(companyId) {
        return CompanyModel.findByIdAndRemove(companyId)
            .then(res => res.toJSON());
    }
}

module.exports = CompanyRepository;
```

Now we can test our work a little we can create or modify index.js.

*./src/index.js*
```javascript
const mongoose = require('mongoose');
const Company = require('./entities/Company');
const PersonRepository = require('./data/mongo/PersonRepository');
const CompanyRepository = require('./data/mongo/CompanyRepository');

(async () => {
    mongoose.connect('mongodb://localhost:27017/test-db');

    let company = new Company('Brand New Company');

    const personRepo = new PersonRepository();
    const companyRepo = new CompanyRepository();

    company = await companyRepo.create(company);
})();
```

Now we have the company created and we can start with some fun. 

We need to code the logic that allows us to hire someone but also to fire an employee if necessary. In this case we can encapsulate this logic to the our repository but in the real world application it will be better to create some kind of service for this.

For the both side reference in MongoDB we have to asign the employee to the company and then asign the company to the employee, because MongoDB does use documents instead of the relational data model. So let's get start with updates.