const { PersonModel } = require('./models/Person');

class PersonRepository {
    create(person) {
        return PersonModel.create(person)
            .then(res => res.toJSON());
    }

    get(personId) {
        return PersonModel.findById(personId)
            .then(res => res.toJSON());
    }

    getAll() {
        return PersonModel.find()
            .then(res => res.map(resItem => resItem.toJSON()));
    }

    update(person) {
        return PersonModel.findByIdAndUpdate(person.id, person, { new: true })
            .then(res => res.toJSON());
    }

    remove(personId) {
        return PersonModel.findByIdAndRemove(personId)
            .then(res => res.toJSON());
    }
}

module.exports = PersonRepository;