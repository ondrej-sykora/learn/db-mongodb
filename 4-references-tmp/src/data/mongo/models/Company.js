const mongoose = require('mongoose');
const { CompanyModelName, PersonModelName } = require('./ModelNames');
const idPlugin = require('../plugins/idPlugin');
var Schema = mongoose.Schema;

const CompanySchema = new Schema({
    name: {
        type: mongoose.SchemaTypes.String,
        required: true
    },
    employees: [{
        type: mongoose.SchemaTypes.ObjectId,
        ref: PersonModelName
    }]
});
CompanySchema.plugin(idPlugin);

const CompanyModel = mongoose.model(CompanyModelName, CompanySchema);

module.exports = {
    CompanyModel: CompanyModel,
    CompanyModelName: CompanyModelName
};