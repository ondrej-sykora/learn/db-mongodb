const { CompanyModel } = require('./models/Company');

class CompanyRepository {
    create(company) {
        return CompanyModel.create(company)
            .then(res => res.toJSON());
    }

    get(companyId) {
        return CompanyModel.findById(companyId)
            .then(res => res.toJSON());
    }

    getAll() {
        return CompanyModel.find()
            .then(res => res.map(resItem => resItem.toJSON()));
    }

    update(company) {
        return CompanyModel.findByIdAndUpdate(company.id, company, { new: true })
            .then(res => res.toJSON());
    }

    delete(companyId) {
        return CompanyModel.findByIdAndRemove(companyId)
            .then(res => res.toJSON());
    }
}

module.exports = CompanyRepository;