const mongoose = require('mongoose');
const Company = require('./entities/Company');
const PersonRepository = require('./data/mongo/PersonRepository');
const CompanyRepository = require('./data/mongo/CompanyRepository');

(async () => {
    mongoose.connect('mongodb://localhost:27017/test-db');

    let company = new Company('Brand New Company');

    const personRepo = new PersonRepository();
    const companyRepo = new CompanyRepository();

    company = await companyRepo.create(company);
})();