# What is MongoDB?

MongoDB is a document database with the scalability and flexibility that you want with the querying and indexing that you need.

* MongoDB stores data in flexible, JSON-like documents, meaning fields can vary from document to document and data structure can be changed over time
* The document model maps to the objects in your application code, making data easy to work with
* Ad hoc queries, indexing, and real time aggregation provide powerful ways to access and analyze your data
* MongoDB is a distributed database at its core, so high availability, horizontal scaling, and geographic distribution are built in and easy to use
* MongoDB is free and open-source, published under the GNU Affero General Public License

> More about MongoDB you can find at the official [MongoDB website](https://www.mongodb.com/).

## Installation

*We will be working with local instance of the MongoDB in this tutorial but if you have some knowledge about that how the MongoDB works or you have your own MongoDB instance (remote or local) you can use your own connection settings as well.*

> You can download the Community Server installation file from the [Download Center](https://www.mongodb.com/download-center) at MongoDB website. 

Once you have MongoDB installed you can try to create connection. I'am running Windows so I have the .exe files installed to the default (C:\Program Files\MongoDB\Server\4.0\bin) path.

> If you have problems with installation, try installing MongoDB without MongoDB Compass. You can install MongoDB Compass separately later. I definitely recommend using MongoDB Compass then MongoDB CLI.

## Startup the database
There are two main files in the bin folder. `mongod.exe` is the build of the MongoDB daemon and `mongo.exe` wich is the CLI tool that allows you to query the running instance of the MongoDB.  

❗ The `mongod.exe` have to be always running when you want to connect to the MongoDB. ❗

> If you don't always want to type complete file path of theese two files try [add the path to you environment variables](https://www.google.cz/search?q=add+path+to+environment+variables&oq=add+path+to+environment+variables). 

Then you can start the MongoDB simply by typing `mongod` into your terminal.

> If **exception in initAndListen: NonExistentPath: Data directory C:\data\db\ not found., terminating** occurs, after that you start the `mongod.exe` file, you should create that particular directory or change MongoDB settings.

You can check that the MongoDB is running by the start `mongo.exe` file in the new terminal window. You should be connected to the default **test** database. So type `db` command and if **test** occurs you are succesfully connected.